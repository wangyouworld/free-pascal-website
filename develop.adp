<master>
<property name="title"><trn key="website.develop.title" locale="en_US">Free Pascal - Development</trn></property>
<property name="entry">develop</property>
<property name="header"><trn key="website.develop.header" locale="en_US">Development</trn></property>

<trn key="website.develop.note" locale="en_US">
<p>
Free Pascal is always under development. If you want to see how the
development is progressing you can take a peek at the developer versions.</p>

<p><b>Note:</b> There is no support for the development versions.</p>
<p><b>Note:</b> Always <b>start using the latest official release</b> when compiling a development version. Any other starting compiler is not guaranteed to work.</p>

<p>You have the following options:</p>
</trn>

<hr>

<a name="sourcesv21"></a>
<a name="sourcestrunk"></a>
<trn key="website.develop.tree" locale="en_US">
<h3>Download Daily Source Snapshot of Development Tree (trunk)</h3>
<p>
You can download today's development (trunk - currently v3.3.x) sources in the form
of a packed source snapshot from our FTP server (and its mirrors). These source snapshots are
updated on a daily basis, and reflect the state of the source repository.
</p>
<p>
Entire fpc sources archive of trunk:
<a href="http://downloads.freepascal.org/fpc/snapshot/trunk/source/fpc.zip">fpc.zip</a> (31 MB).
</p>
<p>
Furthermore, there is an even larger archive including the fpc sources together
with documentation sources and release-building-related files in the same directory -
<a href="http://downloads.freepascal.org/fpc/snapshot/trunk/source/fpcbuild.zip">fpcbuild.zip</a>.
</p>
</trn>

<a name="sourcesv20"></a>
<a name="sourcesfixes"></a>
<trn key="website.develop.snapshot" locale="en_US">
<h3>Download Daily Source Snapshot of the Fixes Tree</h3>
<p>
You can download today's fixes branch (currently v3.2.x) sources in the form
of a packed source snapshot from our FTP server (and its mirrors). These sources
may eventually be used to build the next stable (fixes) release. These source snapshots are updated on
a daily basis, and reflect the state of the source repository.
</p>
<p>
Entire fpc sources archive of the fixes branch:
<a href="http://downloads.freepascal.org/fpc/snapshot/fixes/source/fpc.zip">fpc.zip</a> (31 MB)
</p>
<p>
Furthermore, there is an even larger archive including the fpc sources together
with docs sources and release building related files in the same directory -
<a href="http://downloads.freepascal.org/fpc/snapshot/fixes/source/fpcbuild.zip">fpcbuild.zip</a>.
</p>
</trn>

<br>


<hr>

<a name="snapshotsv21"></a>
<a name="snapshotstrunk"></a>
<trn key="website.develop.daily" locale="en_US">
<h3>Download Daily Update of Development Tree (trunk)</h3>
<p>
These compiled snapshots contain the latest development updates and bug fixes. There is no
guarantee that the new development updates are fully working and that the snapshot
is bugfree.
</p>
<p>The files are available from our <a href="http://downloads.freepascal.org/fpc/snapshot/trunk/">ftp site</a> and mirrors.
</p>
</trn>

<a name="snapshotsv20"></a>
<a name="snapshotsfixes"></a>

<trn key="website.develop.fixes" locale="en_US">
<h3>Download Daily Update of the Fixes Tree</h3>
<p>
These compiled snapshots contain the latest bug fixes, without major new
features. They may be more stable than the development snapshots (and even
than the last official release), but there is still no guarantee that these
snapshots are bug free.
</p>
<p>The files are available from our <a href="http://downloads.freepascal.org/fpc/snapshot/fixes/">ftp site</a> and mirrors.
</p>
</trn>

<br>

<hr>
<trn key="website.develop.git" locale="en_US">
<a name="git"></a><h3>Connect to Source Repository with Git</h3>
<p>
As an alternative to the daily zip files of the FPC sources,
the git repository is accessible for everyone, with read-only access; All
Free pascal related source material is available on
<a href="https://gitlab.com/freepascal.org/fpc">Gitlab</a>:
<pre>
https://gitlab.com/freepascal.org/fpc
</pre>
This means that you can always have access to the latest source code. It is also a method
which requires less bandwidth once you have done the first download (called a
&quot;clone&quot; in git lingo).
</p>
<p>
You can find more information about the FPC gitlab setup and how to use git
in our <a href="https://wiki.freepascal.org/FPC_git">FPC and git Wiki page</a>
</p>
<p>
<b>Development snapshots</b>
</p>
<p>
How do you obtain the sources via git? Generally, you need 3 steps:<br>
(once you have Git installed, of course. Look <a href="https://wiki.freepascal.org/FPC_git#Windows_Git_clients">here</a> for
a list of git clients.)
</p>
</trn>

<trn key="website.develop.fpc" locale="en_US">
<OL>
<li> To retrieve the full fpc source repository,
type
<PRE>
git clone https://gitlab.com/freepascal.org/fpc/source.git fpc
</PRE>
This will create a directory called &quot;fpc&quot; in the current directory, containing
subdirectories with the following components:

<ul>
<li><b>rtl</b>, the run time library source code for all platforms.</li>
<li><b>compiler</b>, the compiler source code.</li>
<li><b>packages</b>, packages source code (contains Free Component Library, gtk, ncurses, mysql and many more)</li>
<li><b>utils</b>, the utilities source code.</li>
<li><b>tests</b>, the compiler and RTL tests.</li>
<li><b>installer</b>, the text mode installer source code.</li>
</ul>

<p>
Normally, you should perform this checkout step just once.
</p>
<li> To update the sources that were downloaded (checked out) above to the latest available version, use
<PRE>
git pull
</PRE>
inside the repository directory.
This command will retrieve patches <em>ONLY</EM> for the files that have
changed on the server. <br>
<p>
You can repeat this step whenever you want to update your sources. It is by
far the most economic way to remain up-to-date in terms of bandwidth.

</OL>
</trn>

<trn key="website.develop.fixes32x" locale="en_US">
<p>
<b>Fixes to 3.2.x </b>
</p>
<p>
The sources of the fixes branch can be checked out in the same directory
using the usual git `checkout` command:
</p>
<pre>
cd fpc
git checkout svn/fixes_3_2
</pre>

and to update:

<pre>
git pull
</pre>

<p>
To checkout a release, you have to checkout the tagged versions, e.g.
<pre>
cd fpc
git checkout release_3_2_2
</pre>
</p>

<p>
The sources of docs are in a separate repository called &quot;documentation&quot;, so the command to get them is
<pre>
git clone https://gitlab.com/freepascal.org/fpc/documentation.git
</pre>

<p>
If you want to learn more about git, read this excellent <a href="https://git-scm.com/book/en/v2">Git book</a>,
which is also available online in different formats for free.
</p>
</trn>

<hr>

<trn key="website.develop.repositories" locale="en_US">
<a name="morerepos"></a><h3>Other repositories</h3>
The Gitlab Freepascal.org group hosts more repositories than only the fpc source repository.
Amongst others the Lazarus project is also hosted there.
Please navigate to the <a href="https://gitlab.com/freepascal.org/"> gitlab
group</a> to see them all.
</trn>

<hr>

<trn key="website.develop.browse" locale="en_US">
<a name="svnweb"></a><h3>Browse the Source Repository with a Web Browser</h3>

<p>
The contents of the git archive can also be browsed with your web-browser by
simply visiting the <a href="https://gitlab.com/freepascal.org/">gitlab group</a>.
</p>
</trn>

<hr>

<trn key="website.develop.future" locale="en_US">
<a name="future"></a><h3>Bugs and the Future</h3>
<p>
Bugs can be reported in the <a href="https://gitlab.com/groups/freepascal.org/fpc/-/issues">Gitlab bugtracker</a>.<br>
Future plans for Free Pascal can be viewed <a href="future@x@">here</a>.<br>
If you are interested in FPC development, you may also be interested in the
<a href="http://wiki.freepascal.org/">wiki</a>.
</p>
</trn>
